module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: [
    '@vue/prettier',
    'plugin:vue/essential',
    'plugin:prettier/recommended'
  ],
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'vue/script-indent': [
      'error',
      2,
      {
        baseIndent: 1
      }
    ],
    'prettier/prettier': [
      'error',
      {
        semi: false,
        singleQuote: true,
        jsxSingleQuote: true,
        vueIndentScriptAndStyle: true
      }
    ]
  },
  parserOptions: {
    parser: 'babel-eslint',
    sourceType: 'module',
    ecmaFeatures: {
      jsx: true,
      modules: true
    }
  }
}
